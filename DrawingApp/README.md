# Requirement

## Python

* Python3 
* Keras
* Flask
* Tensorflow
* OpenCV3

## Javascript

* Node.js

# Installation

1. Install python3 & node.js
2. Install all require package python (with `pip3`)
3. Install module node.js (with `npm i` in directory file)

# How to run

1. Running server (with `sh run.sh` in terminal in directory file)
2. Open browser URL http://localhost:8080
3. Good luck have fun!

# Reference

* Dataset https://www.kaggle.com/c/digit-recognizer/data
* OpenCV3 https://www.pyimagesearch.com/2016/12/05/macos-install-opencv-3-and-python-3-5/
* Tensorflow https://www.tensorflow.org/
* Keras https://keras.io/
* Flask http://flask.pocoo.org/