from MS_Env import *
from datetime import datetime
from Network import *

EPISODE_N = 500
RENDER_ENV = 0
ROW = 4
COL = 4
N_MINE = ROW*COL*20//100 # 20 percent

if __name__ == '__main__':
    env = Minefield(ROW, COL, N_MINE)
    agent = DQNAgent(env)
    agent.load('model_weight.hdf5')
    # print(obs)
    for ep in range(EPISODE_N):
        done = False
        total = 0
        state = env.reset()
        # env.render()
        c = 0
        state = np.reshape(state, [1, ROW*COL])
        print(state)
        break
        while not done:
            index_action = agent.act(state)
            # print(action)
            # action = env.action_space.sample()
            # index_action = env.action_space.sample_index()

            # env.render()

            action = env.action_space.index_to_action(index_action)
            # print(c, action)
            next_state, reward, done_, des = env.step(action)
            next_state = np.reshape(next_state, [1, ROW*COL])
            done = done_

            agent.remember(state, index_action, reward, next_state, done)
            state = next_state
            total += reward
            c += 1
            if done:
                print("==========================================")
                print("episode: {}/{}, score: {}".format(ep+1, EPISODE_N, total))
                # env.render()
                break
        agent.replay(1)
    print(env.Count_Win)
    agent.save('model_weight.hdf5')