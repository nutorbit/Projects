from gym.spaces import Discrete
from gym.spaces.tuple_space import Tuple
from gym.spaces import seed
from gym import spaces
from itertools import product
import numpy as np
import random
import sys
from datetime import datetime

seed()
random.seed(datetime.now())

test_map = {(1, 3), (3, 1), (3, 3), (2, 3)}
'''
    0 0 1 1
    0 0 2 B
    1 1 4 B
    1 B 3 B
'''

class Load_sample:
    def __init__(self, row, col):
        self.row = row
        self.col = col
        x = range(row)
        y = range(col)
        c = range(2) # ['O', 'F'] 0: 'O', 1: 'F'
        self.result = list(product(c, product(x, y)))
        self.free = len(self.result)
        # print(self.result)
        self.n = len(self.result)

    def index_to_action(self, index):
        # print(len(self.result))
        # print(index)
        return self.result[index]

    def sample_index(self):
        # print(self.free)
        return random.randrange(0, self.free)

    def sample(self):
        x = random.choice(self.result)
        # print(x)
        return x

    # update if opened block filter action space
    def update(self, e):
        tmp = []
        # print(self.result)
        for v in self.result:
            if v[1] not in e:
                tmp.append(v)
        # print(self.result)
        self.result = tmp
        # print('-----')
        # print(tmp)
        # print(e)
        # print(result)
        self.free = len(self.result)
        

class Minefield:
    '''
        NOTATION IN BOARD:
            .      | CLOSE BLOCK
            B      | OPEN BOMB
            F      | FLAG BLOCK
            number | BOMB AROUND BLOCK
    '''

    UNKNOWN = -1
    MINE = -9999
    FLAG = -2

    def __init__(self, row=10, col=10, mines=20):
        self.row = row
        self.col = col
        self.mines = mines
        self.nonMines = row*col-mines
        self.state = np.full([self.row, self.col], Minefield.UNKNOWN)
        self.action_space = Load_sample(row, col)
        self.n_actions = 3
        self.observation_space = spaces.Box(np.array([0, 0, 0]), np.array([1, row, col]))
        self.First = True
        self.Count_Win = 0

    def expand_forFirstOpen(self, zeros):
        visited = set()
        MAX = -1
        start = -1
        for key in zeros:
            # print(key)
            s = [(key, 0)]
            # print(s[-1])
            # DFS
            while s: 
                f, r = s[-1][0], s[-1][1]
                s.pop()
                di = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]
                visited.add(f)
                if r > MAX:
                    MAX = r
                    start = key
                for d in di:
                    # print(f[0])
                    tox = f[0]+d[0]
                    toy = f[1]+d[1]
                    if 0 <= tox < self.row and 0 <= toy < self.col:
                        if (tox, toy) not in self.minesCoords and (tox, toy) not in visited:
                            if self.helperField[f[0]][f[1]] == 0:
                                s.append(((tox, toy), r+1))
        # print(start)
        self.expand(start)

    def make_helper(self):
        self.helperField = np.full([self.row, self.col], 0)
        for i in range(self.row):
            for j in range(self.col):
                di = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]
                for d in di:
                    if 0 <= i+d[0] < self.row and 0 <= j+d[1] < self.col and (i+d[0], j+d[1]) in self.minesCoords:
                        self.helperField[i][j] += 1
        # print(self.helperField)
        zeros = dict()
        for i in range(self.row):
            for j in range(self.col):
                if (i, j) in self.minesCoords:
                    self.helperField[i][j] = Minefield.MINE
                if self.helperField[i][j] != Minefield.MINE:
                    zeros[(i, j)] = 0
        self.expand_forFirstOpen(zeros)

    def reset(self):
        self.action_space = Load_sample(self.row, self.col)
        self.minesCoords = set()
        self.opened = set()
        self.state = np.full([self.row, self.col], Minefield.UNKNOWN)
        mines_remain = self.mines
        if self.First:
            while mines_remain > 0:
                r = random.randrange(self.row)
                c = random.randrange(self.col)
                if (r, c) not in self.minesCoords:
                    self.minesCoords.add((r, c))
                    mines_remain -= 1
            self.tmp = set(self.minesCoords)
        else:
            self.minesCoords = set(self.tmp)
            mines_remain = 0
        self.minesCoords = test_map
        self.First = False
        self.mines_remain = self.mines
        self.make_helper() # make dummy table

        # return self.state.reshape((self.row*self.col))
        return self.state


    def expand(self, coord):
        s = [coord]
        # BFS
        e = set()
        while s:
            f = s[0]
            s.pop(0)
            self.state[f[0]][f[1]] = 0 if self.helperField[f[0]][f[1]] == 0 else int(self.helperField[f[0]][f[1]])
            di = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (-1, -1), (1, -1), (-1, 1)]
            self.opened.add((f[0], f[1]))
            e.add((f[0], f[1]))
            for d in di:
                tox = f[0]+d[0]
                toy = f[1]+d[1]
                if 0 <= tox < self.row and 0 <= toy < self.col:
                    if (tox, toy) not in self.minesCoords and (tox, toy) not in self.opened:
                        if self.state[f[0]][f[1]] == 0:
                            s.append((tox, toy))
        self.action_space.update(e)
    
    def step(self, action):
        act, coord = action
        reward = 0
        des = '----'
        done = False
        # action select opened
        if self.state[coord[0]][coord[1]] != Minefield.UNKNOWN:
            # reward -= 2
            self.opened.add((coord[0], coord[1]))
            self.action_space.update(set((coord[0], coord[1])))
        # action open bomb
        elif coord in self.minesCoords and act == 0: # open
            self.state[coord[0]][coord[1]] = Minefield.MINE
            # reward = Minefield.MINE
            done = True
            # print('OPEN BOMB')
            des = 'OPEN BOMB'
        # action flag bomb
        elif coord in self.minesCoords and act == 1: # FLAG
            self.state[coord[0]][coord[1]] = Minefield.FLAG
            self.opened.add((coord[0], coord[1]))
            self.action_space.update(set((coord[0], coord[1])))
            reward += 5
            self.mines_remain -= 1
            des = 'FLAG'
        # action flag block
        elif coord not in self.minesCoords and act == 1:
            if coord not in self.opened:
                self.state[coord[0]][coord[1]] = Minefield.FLAG
                self.opened.add((coord[0], coord[1]))
                self.action_space.update(set((coord[0], coord[1])))
                des = 'FLAG'
            # reward -= 2
        else:
            # print(coord)
            reward += 1
            self.expand(coord) # expand open block
            des = 'OPEN'
        if self.mines_remain == 0:
            done = True
            des = 'WIN'
            # print('WIN')
            reward += 50
            self.Count_Win += 1
        return (self.state, reward, done, des)

    def render(self):
        xAis, yAis = range(self.row), range(self.col)
        # for i in range(self.row):
        #     print(xAis[i], end='|')
        #     for j in range(self.col):
        #         print(self.helperField[i][j] if self.helperField[i][j] != Minefield.MINE else 'x' , end=' ')
        #     print()
        # print('  ', end='') 
        # for i in range(self.col):
        #     print('-', end=' ')
        # print()
        # print('  ', end='')
        # for i in range(self.col):
        #     print(yAis[i], end=' ')
        # print()
        # print('Secret Bomb:', self.minesCoords)
        for i in range(self.row):
            print(xAis[i], end='|')
            for j in range(self.col):
                if self.state[i][j] == Minefield.UNKNOWN:
                    print('.', end=' ')
                elif self.state[i][j] == Minefield.MINE:
                    print('B', end=' ')
                elif self.state[i][j] == Minefield.FLAG:
                    print('F', end=' ')
                else:
                    print(self.state[i][j], end=' ')
            print()
        print('  ', end='')
        for i in range(self.col):
            print('-', end=' ')
        print()
        print('  ', end='')
        for i in range(self.col):
            print(yAis[i], end=' ')
        print()



        